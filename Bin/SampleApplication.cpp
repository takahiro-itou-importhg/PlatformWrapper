//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Platform Wrapper Library  ---                  **
**                                                                      **
**                                                                      **
**          Copyright (C), 2016-2016, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      アプリケーション本体。
**
**      @file       Bin/Sample.cpp
**/

#include    "PlatformWrapper/Common/PlatformTypes.h"

#include    <iostream>

using   namespace   PLATFORM_WRAPPER_NAMESPACE;

int  main(int argc, char * argv[])
{
    std::cout   <<  "Sample Application."   <<  std::endl;
    return ( 0 );
}
