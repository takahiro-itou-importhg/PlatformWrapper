
##
##    List of Tests.
##

EXTRATESTS              =
TESTS                   =  \
        PlatformSettingsTest  \
        $(EXTRATESTS)

##
##    Compile and Link Options.
##

TESTCPPFLAGS            +=
TESTLDFLAGS             +=
TESTLIBTESTEE           +=

##
##    Test Configurations.
##

##
##    Test Programs.
##

PlatformSettingsTest_SOURCES    =  PlatformSettingsTest.cpp

