//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Platform Wrapper Library  ---                  **
**                                                                      **
**                                                                      **
**          Copyright (C), 2016-2016, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      プロジェクトの設定。
**
**      @file       Common/PlatformSettings.h
**/

#if !defined( PFWRAPPER_COMMON_INCLUDED_PLATFORM_SETTINGS_H )
#    define   PFWRAPPER_COMMON_INCLUDED_PLATFORM_SETTINGS_H

//  スクリプトによる設定値が書き込まれたヘッダを読み込む。  //
#include    "PlatformWrapper/.Config/ConfiguredPlatformWrapper.h"

PLATFORM_WRAPPER_NAMESPACE_BEGIN

PLATFORM_WRAPPER_NAMESPACE_END

#endif
