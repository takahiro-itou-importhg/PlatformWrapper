#! /bin/bash  -x

mkdir  -p  .Config

chmod  u+x  setuplinks.sh
./setuplinks.sh

aclocal  -I  .Config  \
  &&  autoheader      \
  &&  automake  --add-missing  --copy  --foreign  \
  &&  autoconf

if [ -x  checkperms.sh ] ; then
  ./checkperms.sh
fi

